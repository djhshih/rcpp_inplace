
```r
library(Rcpp);
sourceCpp("clear.cpp");
```

A function `clear_matrix` is defined in `clear.cpp` and it sets all elemenents of the input numeric matrix to `-1.0`.

If the input matrix is numeric, `clear_matrix`, which accepts `NumericMatrix` will modify the matrix in place.

If the input matrix is not numeric (i.e.) integer, a copy of the matrix will be made automatically. Therefore, any changes by `clear_matrix` will not affect the parent scope.


### Initialization with scalar


```r
x0 <- matrix(1, 3, 4);
storage.mode(x0);
```

```
## [1] "double"
```

```r
is.integer(x0);
```

```
## [1] FALSE
```

```r
print(x0);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    1    1    1
## [2,]    1    1    1    1
## [3,]    1    1    1    1
```

```r
clear_matrix(x0);
print(x0);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]   -1   -1   -1   -1
## [2,]   -1   -1   -1   -1
## [3,]   -1   -1   -1   -1
```
x0 is modified in-place.

### Initialization with vector created with `rep`


```r
x1 <- matrix(rep(1, 12), 3, 4);
storage.mode(x1);
```

```
## [1] "double"
```

```r
is.integer(x1);
```

```
## [1] FALSE
```

```r
print(x1);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    1    1    1
## [2,]    1    1    1    1
## [3,]    1    1    1    1
```

```r
clear_matrix(x1);
print(x1);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]   -1   -1   -1   -1
## [2,]   -1   -1   -1   -1
## [3,]   -1   -1   -1   -1
```
x1 is modified in-place.


### Initialization with vector created with `:`


```r
x2 <- matrix(1:12, 3, 4);
storage.mode(x2);
```

```
## [1] "integer"
```

```r
is.integer(x2);
```

```
## [1] TRUE
```

```r
print(x2);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    4    7   10
## [2,]    2    5    8   11
## [3,]    3    6    9   12
```

```r
clear_matrix(x2);
print(x2);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    4    7   10
## [2,]    2    5    8   11
## [3,]    3    6    9   12
```
x2 is not modified in-place.

### Initialization with hard-coded vector


```r
x3 <- matrix(c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12), 3, 4);
storage.mode(x3);
```

```
## [1] "double"
```

```r
is.integer(x3);
```

```
## [1] FALSE
```

```r
print(x3);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    4    7   10
## [2,]    2    5    8   11
## [3,]    3    6    9   12
```

```r
clear_matrix(x3);
print(x3);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]   -1   -1   -1   -1
## [2,]   -1   -1   -1   -1
## [3,]   -1   -1   -1   -1
```
x3 is modified in-place.

### Initialization with `sample`


```r
x4 <- matrix(sample(1:12), 3, 4);
storage.mode(x4);
```

```
## [1] "integer"
```

```r
is.integer(x4);
```

```
## [1] TRUE
```

```r
print(x4);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    6    5    9
## [2,]    7    2    4    8
## [3,]   10   12    3   11
```

```r
clear_matrix(x4);
print(x4);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    6    5    9
## [2,]    7    2    4    8
## [3,]   10   12    3   11
```
x4 is not modified in-place.

### Initialization with hard-coded vector (out-of-order)


```r
x5 <- matrix(c(3, 2, 1, 3, 5, 3, 2, 5, 2, 11, 10, 12), 3, 4);
storage.mode(x5);
```

```
## [1] "double"
```

```r
is.integer(x5);
```

```
## [1] FALSE
```

```r
print(x5);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    3    3    2   11
## [2,]    2    5    5   10
## [3,]    1    3    2   12
```

```r
clear_matrix(x5);
print(x5);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]   -1   -1   -1   -1
## [2,]   -1   -1   -1   -1
## [3,]   -1   -1   -1   -1
```
x5 is modified in-place.

### Initialization by copy


```r
x6 <- x2;
storage.mode(x6);
```

```
## [1] "integer"
```

```r
is.integer(x6);
```

```
## [1] TRUE
```

```r
print(x6);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    4    7   10
## [2,]    2    5    8   11
## [3,]    3    6    9   12
```

```r
clear_matrix(x6);
print(x6);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    4    7   10
## [2,]    2    5    8   11
## [3,]    3    6    9   12
```
x6 is not modified in-place.

### Initialization by copy with subsequent modification


```r
x7 <- x2;
x7[2,2] <- 3;
storage.mode(x7);
```

```
## [1] "double"
```

```r
is.integer(x7);
```

```
## [1] FALSE
```

```r
print(x7);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    4    7   10
## [2,]    2    3    8   11
## [3,]    3    6    9   12
```

```r
clear_matrix(x7);
print(x7);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]   -1   -1   -1   -1
## [2,]   -1   -1   -1   -1
## [3,]   -1   -1   -1   -1
```
x7 is modified in-place.
After having modified one element, x7 is no longer an integer matrix  and it is is now modifiable by `clear_matrix`.


```r
x8 <- x2;
storage.mode(x8);
```

```
## [1] "integer"
```

```r
is.integer(x8);
```

```
## [1] TRUE
```

```r
print(x8);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    4    7   10
## [2,]    2    5    8   11
## [3,]    3    6    9   12
```

```r
clear_matrix(x8);
print(x8);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]    1    4    7   10
## [2,]    2    5    8   11
## [3,]    3    6    9   12
```

```r
clear_integer_matrix(x8);
print(x8);
```

```
##      [,1] [,2] [,3] [,4]
## [1,]   -1   -1   -1   -1
## [2,]   -1   -1   -1   -1
## [3,]   -1   -1   -1   -1
```
x8 is not modified in-place by `clear_matrix`, which takes a NumericMatrix, but it is modified in-place by `clear_integer_matrix`, which takes a IntegerMatrix.

### Session information


```r
sessionInfo();
```

```
## R version 3.1.1 (2014-07-10)
## Platform: x86_64-apple-darwin13.1.0 (64-bit)
## 
## locale:
## [1] en_CA.UTF-8/en_CA.UTF-8/en_CA.UTF-8/C/en_CA.UTF-8/en_CA.UTF-8
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] Rcpp_0.11.3         knitr_1.6           devtools_1.6.0.9000
## 
## loaded via a namespace (and not attached):
## [1] evaluate_0.5.5 formatR_1.0    stringr_0.6.2  tools_3.1.1
```

