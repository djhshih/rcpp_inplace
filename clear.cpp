#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
void clear_matrix(NumericMatrix X) {
	size_t m = X.nrow(), n = X.ncol();
	for (size_t j = 0; j < n; ++j) {
		for (size_t i = 0; i < m; ++i) {
			X(i, j) = -1.0;
		}
	}
}

// [[Rcpp::export]]
void clear_integer_matrix(IntegerMatrix X) {
	size_t m = X.nrow(), n = X.ncol();
	for (size_t j = 0; j < n; ++j) {
		for (size_t i = 0; i < m; ++i) {
			X(i, j) = -1.0;
		}
	}
}


